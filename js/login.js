function statusChangeCallback(response) {
  console.log('statusChangeCallback');
  console.log(response);
  if (response.status === 'connected') {
    testAPI(response.authResponse.accesstoken);
  } else {
    document.getElementById('status').innerHTML = 'Please log ' +
      'into this webpage.';
  }
}

function checkLoginState() {
  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });
}

window.fbAsyncInit = function() {
  FB.init({
    appId            : '1144541892667745',
    autoLogAppEvents : true,
    xfbml            : true,
    version          : 'v10.0'
  });
};

function testAPI(fb_token) {
  console.log('Welcome!  Fetching your information.... ');
  FB.api('/me', function(response) {
    console.log('Successful Facebook login for: ' + response.name + ',' + response.id);
    var xhr = new XMLHttpRequest();
    var url = "https://dev.biclomap.com/api/login/facebook";
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onload = function () {
      if (xhr.status === 200) {
        var json = JSON.parse(xhr.responseText);
        console.log("Successful Biclomap login");
      }
    };
    var data = JSON.stringify({"name": response.name, "id": response.id});
    xhr.send(data);
  });
}

