---
layout: page
title: Biclomap Privacy Policy
permalink: /privacy/
---

Biclomap is a community which has been formed by its collective members for
the purpose of the development and the promotion of the Mountain Bike and
other related activities. This policy governs how the information which may be
collected from or provided by you will be held and used as part of us
providing services to you which allow you to participate in the Biclomap
community.

## Purpose

We collect personal information in order to provide you with services on our
websites. This information is collected only where necessary to deliver those
services as outlined below. For instance, in order to be able to join an
Mountain Bike Tour, you are required to create a Biclomap Identity account. We
requires this in order to send you notifications when others share MTB tracks
with you.

## Updating your personal information

In most cases our services provide the ability for you to update your
information on a self-service basis. To do this, please follow the steps below
as noted for each individual service.

Should you have any issues following them, or have other questions which are
not answered below, please get in touch with us as detailed in the Contact
Information section.

## What information do we collect?

### Information collected is dependent on the service

The information we collect from you will vary depending on the service being
accessed, the level of access you hold on that service and the nature of your
interactions with that service.

With the exception of aggregated website statistics, anonymous donations and
regular server logging, no information is collected from people accessing our
services over the web who have not logged into them.

### Aggregated website statistics

The Biclomap Community do not collect any website statistics.

### Regular server logging

In order to protect the security of our systems, and to assist us in ensuring
the smooth and uninterrupted provision of service to the Biclomap Community our
systems automatically log every request they receive.

This logging includes the full IP address and user agent of the requestor,
along with the referring page which sent them to us and the nature of the
request they made to us.

These logs are only made accessible to system administrators who have
responsibilty for the smooth operation of our services, and are automatically
removed from our systems after 14 days.

### Biclomap Identity

To ease the use of our services, the Biclomap Community offers a service which
allows for users to register once for access to a number of our services.
During this process, you’ll be asked to provide your name and email address,
along with a password, and be given the choice of a series of usernames. Once
confirmed, your account will then be created and you’ll be able to access
services which use Biclomap Identity for authentication.

Should this information need to be updated at any time, this can be done in a
self-service manner through the Biclomap Web site. As part of this process,
you’ll also have the option of providing us with optional additional
information should you wish.

Profiles created on the KDE Identity platform are not made publicly
accessible, with the exception of Biclomap Developers, whose name and email
address are made accessible to the public.

Information is not transferred from KDE Identity to those services until you
login to them for the first time.

### Bug Tracking System

Participation on the Biclomap Bug Tracking System, powered by Gitlab, requires
registration of a separate account. That account is maintained and managed by
Gitlab. Please refer to their privacy policy.

### Source Code Repositories

As an open source software development community, Biclomap produces a large
quantity of source code, which we store in Source Code Management systems
known as Git. These systems store our code and the changes to it in records
known as commits, which include the change to the code, an annotation from the
developer (commit message), the time and date of the change and the identity
of the person making the change. For Git, the identity information is stored
in the form of the name and email address, both of the person committing the
change to the repository, as well as the person who authored the change
originally where that is different.

As part of the process of entering commits into our repositories, various
actions may be automatically taken. This includes sending of emails concerning
the commits to public mailing lists (which are archived by third parties) as
well as directly to individuals named in the commits or who have otherwise
registered their interest with us in regard to certain forms of changes taking
place in our repositories. It may also include notifications being sent to
Gitlab, which may result in bugs, tasks or code reviews being closed, with
reference being made to the commit in question, and the people and mailing
lists affected by those being informed of the change and details of it in
turn. Following this the commits may also be indexed by systems such as
Gitlab, and be made publicly available  for anyone to access.

By entering commits into our repositories you acknowledge that the information
contained in them will form part of the public record of open source
development activity undertaken by the Biclomap Community, and that these records
constitute part of the historical record of our community. As a consequence of
them being historical records, it is not possible to rectify them once they
have been entered into our repositories as this would constitute a
modification of the historical record, which is also prevented by
cryptographic security mechanisms built in Git. Further, as computer source
code is a creation in which intellectual property rights (copyright) are held
by the author, removal of those records is not possible as we are legally
required to keep records on who was responsible for authoring changes in the
code we hold.

## Changes in the Pivacy Policy

We may change the privacy policy when we add new services, retire existing
services and update existing ones. When this is done, except where the updates
are minor (such as correcting typographical errors) we will post an
announcement regarding the update on our website and detail the impact of the
changes on you.

## Contact information

Should you have any queries or other matters which aren’t addressed above,
please feel free to contact us as described below.

For queries relating to updating or removing information, or relating to how
our services operate and publish data, please contact our team. This can be
done by sending an email to <a
href="mailto:bikers@biclomap.com">bikers@biclomap.com</a>.

