server {
	listen       80;
	server_name  dev.biclomap.com;
	root         /data/nginx/dev.biclomap.com;
#	index index.php index.html;

# letsencrpyt
	location /.well-known/acme-challenge/ {
		root        /data/nginx/letsencrypt;
	    default_type text/plain;
	}
	location  / {
		return 	301 https://$server_name$request_uri;
	}
}


server {
	listen      443 ssl;
	server_name dev.biclomap.com;
	index index.html;

	ssl_certificate      /etc/letsencrypt/live/dev.biclomap.com/fullchain.pem;
	ssl_certificate_key  /etc/letsencrypt/live/dev.biclomap.com/privkey.pem;
	ssl_protocols       TLSv1 TLSv1.1 TLSv1.2;

	root    /data/nginx/dev.biclomap.com;
	charset utf-8;

	location '/.well-known/acme-challenge' {
		default_type "text/plain";
		root        /data/nginx/letsencrypt;
	}

	location /api/ {
		access_log /var/log/nginx/biclomap_api.log main;

		proxy_set_header X-Real-IP $remote_addr; 
# adjust the path to the current AWS API Gateway
		proxy_pass https://a32c0dxfi6.execute-api.eu-central-1.amazonaws.com/dev/;
		proxy_ssl_server_name on;
		proxy_ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
		proxy_buffering off;
	}

}
